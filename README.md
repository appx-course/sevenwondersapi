# Seven Wonders API

This repository stores the Seven Wonders 'API' for the APP3x project. It was
intended to be hosted on BitBucket Pages but this was too difficult to sort
out quickly. As such, it's now hosted at <https://appx.haxx.net.au/.>

## Document Versioning

Last modified by Tristan Reed, 6 Jan 2020.
